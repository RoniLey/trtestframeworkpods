Pod::Spec.new do |s|
  s.name = 'TruliooDocVBeta'
  s.version = '2.9.4'
  s.swift_versions = '4.0'
  s.author = 'Trulioo, Inc.'
  s.license = { :type => 'Custom', :text => 'Please see LICENSE.md file.' }
  s.homepage = 'https://www.trulioo.com/'
  s.source = { :http => 'https://superal-sdk.verification.trulioo.io/2.9.4/TruliooDocV.zip', :flatten => true }
  s.summary = 'Trulioo DocV iOS SDK'
  s.documentation_url = 'https://docs.verification.trulioo.com/sdk/ios/index.html'
  s.dependency 'lottie-ios', '~> 4.3.3'
  s.ios.vendored_frameworks = 'TruliooCaptureSDK.xcframework', 'TruliooCore.xcframework'
  s.ios.deployment_target = '14.0'
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'x86_64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'x86_64' }
end
