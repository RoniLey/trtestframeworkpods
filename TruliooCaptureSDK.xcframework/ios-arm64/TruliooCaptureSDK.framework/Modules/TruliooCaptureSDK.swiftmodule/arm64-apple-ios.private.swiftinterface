// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.9.2 (swiftlang-5.9.2.2.56 clang-1500.1.0.2.5)
// swift-module-flags: -target arm64-apple-ios14.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -module-name TruliooCaptureSDK
// swift-module-flags-ignorable: -enable-bare-slash-regex
import AVFoundation
import Combine
import CoreML
import Darwin
import DeveloperToolsSupport
import Foundation
import Lottie
import QuartzCore
import Swift
import SwiftUI
import UIKit
import Vision
import _Concurrency
import _StringProcessing
import _SwiftConcurrencyShims
public enum TruliooLiteViews : Swift.String, Swift.CaseIterable {
  case documentCaptureFront
  case documentCaptureBack
  case selfieCapture
  public init?(rawValue: Swift.String)
  public typealias AllCases = [TruliooCaptureSDK.TruliooLiteViews]
  public typealias RawValue = Swift.String
  public static var allCases: [TruliooCaptureSDK.TruliooLiteViews] {
    get
  }
  public var rawValue: Swift.String {
    get
  }
}
public protocol PTruliooResult : Swift.Encodable {
  var transactionId: Swift.String { get }
}
public struct TruliooResult : TruliooCaptureSDK.PTruliooResult, Swift.Encodable {
  public let transactionId: Swift.String
  public let status: TruliooCaptureSDK.TruliooStatus
  public func encode(to encoder: any Swift.Encoder) throws
}
public struct TruliooSuccess : TruliooCaptureSDK.PTruliooResult, Swift.Encodable {
  public let transactionId: Swift.String
  public func encode(to encoder: any Swift.Encoder) throws
}
public struct TruliooError : TruliooCaptureSDK.PTruliooResult, Swift.Encodable {
  public let transactionId: Swift.String
  public let message: Swift.String
  public let code: Swift.Int32
  public func encode(to encoder: any Swift.Encoder) throws
}
public struct TruliooException : TruliooCaptureSDK.PTruliooResult, Swift.Encodable {
  public let transactionId: Swift.String
  public let message: Swift.String
  public func encode(to encoder: any Swift.Encoder) throws
}
public enum TruliooStatus : Swift.Encodable {
  case ACCEPTED, REVIEW, DECLINED, ERROR
  public static func == (a: TruliooCaptureSDK.TruliooStatus, b: TruliooCaptureSDK.TruliooStatus) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public func encode(to encoder: any Swift.Encoder) throws
  public var hashValue: Swift.Int {
    get
  }
}
public protocol TruliooDelegate {
  func onInitialized()
  func onError(error: TruliooCaptureSDK.TruliooError)
  func onException(exception: TruliooCaptureSDK.TruliooException)
  func onComplete(result: TruliooCaptureSDK.TruliooSuccess)
}
public struct TruliooWorkflowTheme {
  public let logoAssetName: Swift.String?
  public let primaryButtonColorName: Swift.String?
  public let primaryButtonTextColorName: Swift.String?
  public init(logoAssetName: Swift.String?, primaryButtonColorName: Swift.String?, primaryButtonTextColorName: Swift.String?)
}
public struct TruliooWorkflow {
  public let shortCode: Swift.String
  public let locale: Swift.String?
  public let theme: TruliooCaptureSDK.TruliooWorkflowTheme?
  public let isDemoMode: Swift.Bool
  public let enableRegionSelection: Swift.Bool
  public let enableDocumentAutoCapture: Swift.Bool
  public init(shortCode: Swift.String, locale: Swift.String? = nil, theme: TruliooCaptureSDK.TruliooWorkflowTheme? = nil, isDemoMode: Swift.Bool = false, enableRegionSelection: Swift.Bool = true, enableDocumentAutoCapture: Swift.Bool = true)
}
@_inheritsConvenienceInitializers @objc public class TruliooLite : ObjectiveC.NSObject {
  @objc override dynamic public init()
  public func initialize(delegate: any TruliooCaptureSDK.TruliooDelegate, workflow: TruliooCaptureSDK.TruliooWorkflow)
  public func launchCapture(delegate: any TruliooCaptureSDK.TruliooDelegate, view: TruliooCaptureSDK.TruliooLiteViews)
  @objc public func oldSchoolLaunch(view: UIKit.UIView)
  public func close()
  @_Concurrency.MainActor public func getSessionResult(onComplete: @escaping (TruliooCaptureSDK.TruliooResult) -> Swift.Void)
  @objc deinit
}
extension TruliooCaptureSDK.TruliooLiteViews : Swift.Equatable {}
extension TruliooCaptureSDK.TruliooLiteViews : Swift.Hashable {}
extension TruliooCaptureSDK.TruliooLiteViews : Swift.RawRepresentable {}
extension TruliooCaptureSDK.TruliooStatus : Swift.Equatable {}
extension TruliooCaptureSDK.TruliooStatus : Swift.Hashable {}
